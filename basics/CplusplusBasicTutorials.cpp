#include ".\intro\introex01.hpp"
#include ".\intro\introex02.hpp"
#include ".\intro\introex03.hpp"
#include ".\intro\introex04.hpp"
#include ".\intro\introex05.hpp"
#include ".\intro\introex06.hpp"
#include ".\intro\introex07.hpp"

int main()
{
	int x;
	bool automatic;
	std::cout << "Exercise runner" << std::endl << "Select Exercise (number)" << std::endl;
	std::cin >> x;
	
	switch(x)
	{
		case 1:
		{
            IntroEx01Test();
            break;
        }
		case 2:
		{
			IntroEx02Test();
			break;
		}
		case 3:
		{
			IntroEx03Test();
			break;
		}
		case 4:
		{
			IntroEx04Test();
			break;
		}
 		case 5:
		{
			IntroEx05Test();
			break;
		}
		case 6:
		{
			IntroEx06Test();
			break;
		}
		case 7:
		{
			IntroEx07Test();
			break;
		}
    }
}