#include "introex07.hpp"

int CalcPrecisionValue(int input);

void IntroEx07Test()
{
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			<< "Excercise07" << std::endl;;
	IntroEx07(46, 78);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			<< "Excercise07" << std::endl;;
	IntroEx07(4642, 7813);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx07(int val1, int val2)
{
	std::cout << "Values: " << val1 << " and " << val2 << std::endl;
	double result = (val1+val2) / (double)13;
	std::cout << "Result: " << std::setprecision(CalcPrecisionValue((int)result)) << result << std::endl;
}

int CalcPrecisionValue(int input)
{
	int digitcounter = 1;
	bool reached = false;
	int devider = 10;
	while (!reached)
	{
		if(input / devider > 0)
		{
			devider *= 10;
			digitcounter++;
		}
		else
		{
			reached = true;
		}
	}
	return digitcounter + 4;
}