#include "introex01.hpp"
#include "introex04.hpp"

void IntroEx04Test()
{
	std::stringstream ss;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			<< "Excercise04" << std::endl;
	ss << 100 << ' ' << 200; 
	IntroEx04(ss);
	std::stringstream().swap(ss);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise04" << std::endl;
	ss << 100 << ' ' << 100;
	IntroEx04(ss);
	std::stringstream().swap(ss);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise04" << std::endl;
	ss << 200 << ' ' << 100;
	IntroEx04(ss);
	std::stringstream().swap(ss);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise04" << std::endl;
	ss << 100 << ' ' << "something";
	IntroEx04(ss);
	std::stringstream().swap(ss);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise04" << std::endl;
	ss << "something" << ' ' << 100;
	IntroEx04(ss);
	std::stringstream().swap(ss);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx04(std::stringstream& ass)
{
	std::string input;
	ass >> input;
	int value1 = InputParser(input);
	ass >> input;
	int value2 = InputParser(input);

	value2 -= value1;
	if(0 > value2 || 0 > value1)
	{
		std::cout << "Invalid Price interval edges." << std::endl;
	}
	else
	{
		std::cout << "Given Price intervall: " << value2 << std::endl;
	}
}