#include "introex01.hpp"
#include "introex03.hpp"

void IntroEx03Test()
{
    std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise03" << std::endl;
	std::stringstream ss1;
	ss1 << 100 << ' ' << 200 << ' ' << 300 << ' ' << 299;
	IntroEx03(ss1);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise03" << std::endl;
	std::stringstream ss2;
	ss2 << 100 << ' ' << 200 << ' ' << "notnumber" << ' ' << 299;
	IntroEx03(ss2);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise03" << std::endl;
	std::stringstream ss3;
	ss3 << 100 << ' ' << 200 << ' ' << 300 << ' ' << 301;
	IntroEx03(ss3);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx03(std::stringstream& ass)
{
	int previousBid = 0;
	bool underbid = false;
	while(!underbid)
	{
		std::string input;
		ass >> input;
		int newBid = InputParser(input);
		if(previousBid < newBid)
		{
			std::cout << "New bid value: " << newBid << std::endl;
			previousBid = newBid;
		}
		else
		{
			std::cout << "No valid new bid. End of biddig." << std::endl;
			underbid = true;
		}
	}
}