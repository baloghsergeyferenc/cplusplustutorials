#include "introex01.hpp"
#include "introex05.hpp"

void IntroEx05Test()
{
	std::stringstream ss;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			<< "Excercise05" << std::endl;
	ss << "joe" << ' ' << 100 << ' ' << "jane" << ' ' << 200;
	IntroEx05(ss);
	std::stringstream().swap(ss);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx05(std::stringstream& ass)
{
	std::string input;
	std::pair<std::string, int> firstPerson;
	std::pair<std::string, int> secondPerson;
	ass >> firstPerson.first;
	ass >> input;
	firstPerson.second = InputParser(input);
	ass >> secondPerson.first;
	ass >> input;
	secondPerson.second = InputParser(input);

	std::cout << "---Original---" << std::endl;
	std::cout << firstPerson.first << " had number: " << firstPerson.second << std::endl;
	std::cout << secondPerson.first << " had number: " << secondPerson.second << std::endl;
	
	int temporary = firstPerson.second;
	firstPerson.second = secondPerson.second;
	secondPerson.second = temporary;

	std::cout << "---New---" << std::endl;
	std::cout << firstPerson.first << " has number: " << firstPerson.second << std::endl;
	std::cout << secondPerson.first << " has number: " << secondPerson.second << std::endl;
}