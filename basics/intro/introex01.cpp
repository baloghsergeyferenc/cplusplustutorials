#include "introex01.hpp"

void IntroEx01Test()
{
	std::cout<< "--------------------OUTPUTS-START--------------------" << std::endl
			 	<< "Excercise01" << std::endl
				<< "Write input - Input is 5." << std::endl;
				IntroEx01("125");
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout 	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise01" << std::endl
				<< "Write input - Input is -3." << std::endl;
				IntroEx01("-3");
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			 	<< "Excercise01" << std::endl
				<< "Write input - Input is 'notnumber'." << std::endl;
				IntroEx01("notnumber");
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx01(std::string input)
{
	int x = InputParser(input);
	if(x < 0)
	{
		std::cout << "Input is invalid or negative." << std::endl;
	}
	else
	{
		std::cout << "Input is " << x << std::endl;

	}
}

int InputParser(std::string input)
{
	int x;
	try
	{
		x = std::stoi(input);
	}
	catch(const std::exception& e )
	{
		std::cout 	<< "Input is not a number: " << input << std::endl
			<< "Excteption thrown with value: " << e.what() << std::endl;
		x = -1;
	}
	return x;
}
