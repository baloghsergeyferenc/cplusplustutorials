#include "introex01.hpp"
#include "introex02.hpp"

void IntroEx02Test()
{
    std::cout<< "--------------------OUTPUTS-START--------------------" << std::endl
			 	<< "Excercise02" << std::endl
				<< "Write input - Input is 5." << std::endl;
				IntroEx02("125");
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout 	<< "--------------------OUTPUTS-START--------------------" << std::endl
				<< "Excercise02" << std::endl
				<< "Write input - Input is -3." << std::endl;
				IntroEx02("-3");
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			 	<< "Excercise02" << std::endl
				<< "Write input - Input is 'notnumber'." << std::endl;
				IntroEx02("notnumber");
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx02(std::string input)
{
    int x = InputParser(input);
    if(0 < x)
    {
        std::cout << "Credit value agerage: " << (double)x/30 << std::endl;
    }
    else
    {
        std::cout << "No calculation because of invalid input." << std::endl;
    }
}