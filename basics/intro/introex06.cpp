#include "introex06.hpp"

void IntroEx06Test()
{
	std::cout	<< "--------------------OUTPUTS-START--------------------" << std::endl
			<< "Excercise06" << std::endl;;
	IntroEx06(56703);
	std::cout	<< "--------------------OUTPUTS-STOP--------------------" << std::endl;
}

void IntroEx06(int income)
{
	std::cout << "Income: " << income << std::endl;
	std::cout << "Spent: " << income % 5000 << std::endl;
	std::cout << "Invested $5000 priced value paper: " << income / 5000 << std::endl;
}